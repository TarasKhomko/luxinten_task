<?php

define('DS', DIRECTORY_SEPARATOR);
define("ROOT", dirname(__FILE__));
define('APP_DIR', realpath('App'));
define('CONTROLLER_DIR', realpath('App/Controllers'));
define('TEMPLATE_DIR', realpath('App/Tamplates'));
define('CONFIG_PATH', realpath('config'));
define('PHP_EXTENSION','.php');
define('TAMPLATE_EXTENSION','.php');
require_once 'Application.php';