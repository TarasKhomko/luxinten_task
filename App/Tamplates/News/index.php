
<?php
require_once (ROOT . "/Tamplates/layouts/header.php");
?>

<div class="container">
    <div class="r4ow">
        <?php foreach ($params as $post): ?>
            <div class="card">

                <h5 class="card-header">
                    <a href="/luxinten_task/posts/find/<?= $post['id'] ?>" class="card-link"><?= $post['title'] ?></a>
                </h5>

                <div class="card-body">

                    <p><?= $post['article'] ?></p>
                </div>
            </div>                   
        <?php endforeach; ?>
    </div>
</div>
<?php
require_once (ROOT . "/Tamplates/layouts/footer.php");
