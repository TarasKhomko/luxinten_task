
<?php
require_once (ROOT . "/Tamplates/layouts/header.php");
?>
<div class="container">
    <div class="r4ow">

        <div class="card">
            <div class="card-body">
                <form id="form-post" method="POST" action="/luxinten_task/posts/update" enctype="multipart/form-data">     
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Title</label>
                        <input type="text" name="title" class="form-control" id="exampleFormControlInput1" value="<?= $title ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Article</label>
                        <textarea  class="form-control" id="exampleFormControlTextarea1" rows="3" name="article"  required><?= $article ?>
                        </textarea>
                    </div>
                    <button type="submit" id="submit" name="id" value="<?= $id ?>"  class="btn btn-primary btn-block">Add post</button>
                </form>
            </div>
        </div>                   
    </div>
</div>
<?php
require_once (ROOT . "/Tamplates/layouts/footer.php");


