<?php
require_once (ROOT . "/Tamplates/layouts/header.php");
?>
<div class="container">
    <div class="r4ow">

        <div class="card">
            <h5 class="card-header">
                <?= $title ?>
            </h5>
            <div class="card-body">

                <p><?= $article ?></p>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="/luxinten_task/" class="btn btn-primary">Go back</a>
                    <a href="/luxinten_task/posts/edit/<?= $id ?>" class="btn btn-info">Edit</a>
                    <a class="btn btn-danger" href="/luxinten_task/posts/delete/<?= $id ?>" role="button">Delete</a>
                </div>
            </div>
        </div>                   

    </div>
</div>
<?php
require_once (ROOT . "/Tamplates/layouts/footer.php");
