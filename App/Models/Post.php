<?php

namespace App\Models;

use App\Core\Database;

class Post {

    function all() {
        $conn = Database::connect();
        $connect = $conn->prepare('SELECT * FROM posts ORDER BY id DESC');
        $connect->execute();
        $result = $connect->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    function find($id) {
        $conn = Database::connect();
        $connect = $conn->prepare("SELECT * FROM posts WHERE id ='$id[0]'");
        $connect->execute();
        $result = $connect->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    function create($data) {
        $conn = Database::connect();
        $connect = $conn->prepare("INSERT INTO posts(title, article)VALUES(?, ?)");
        $connect->execute($data);
        return true;
    }

    function update($data) {
        $title = $data['title'];
        $article = $data['article'];
        $id = $data['id'];

        $conn = Database::connect();
        $connect = $conn->prepare("UPDATE posts SET title=:title, article=:article WHERE id=:id   ");

        $connect->execute($data);
        return true;
    }

    function delete($id) {
        $conn = Database::connect();
        $conn->exec("DELETE FROM posts WHERE id= '$id[0]'");
    }

}
