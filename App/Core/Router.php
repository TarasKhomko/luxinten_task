<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Core;
use App\Core\Factory;
use App\Core\Response;
use App\Core\Request;

/**
 * Description of Router
 *
 * @author taras
 */
class Router 
{
    private $controller = 'Posts';
	private $action = 'index';
	private $route = '';
	private $reg_paths = array(
		'/\/([a-z0-9+_\-]+)\/([a-z0-9+_\-]+)/' => 'controller/action',
		'/^\/([a-z0-9+_\-]+$)/' => 'controller',
		);
	private static $out;
	public static function create(string $method, string $route)
	{
                 
                return new self($method, $route);
	}
        
        public static function getOut()
        {
               
                return self::$out;
        }

    private function __construct(string $method, string $route)
	{
		
                $this->method = $method;
        		$this->route = $route;
        		$this->run();
	}

	private function run()
	{
		      $this->parse(substr( $this->route , 15)); 
              if (strlen((substr( $this->route , 15))) == 0) {
                 $controller = $this->controller;
                 $action = $this->action;
                  $args = [];
              } 
              else{
                $url =   substr($this->route , 15); 
                $arrUrl =  explode("/", $url); 
                $controller = ucfirst(array_shift($arrUrl)); 
                $action =   ucfirst(array_shift($arrUrl));
                $args = $arrUrl;

              }
            

             
                
                $controllers = self::getControllers();                         
                if(empty($controllers)){
                    echo '1';
                    throw new \Exception('There are no controllers');
                }
                if(!$this->getMatchController($controllers, $controller)){
                    echo '2';
                    Response::error404();
                    return false;
                }
                if(!$this->applyController($controller)){
                    echo '3';
                    throw new \Exception('Controller file was not found');
                }

                
                 $namespace = $this->getControllerNamespace($controller);                         
                //$namespace = $this->getControllerNamespace($this->controller);
                if(!\is_callable(array($namespace, $action))){
                    Response::error404();
                    return false;
                } 
                             
                $controller = Factory::create($namespace);
                static::$out = $this->call([$controller, $action],[$args]);
                return true;
        }
            
            private function call($callback = [], $args=[])
        {
              
            return \call_user_func_array($callback, $args);
        }
        
        private function prepareForNs(string $what):string
        {
         
            return "App\\Controllers\\". ucfirst($what);
        }

                private function prepareForPath(string $controllerName):string
        {
                  
            return\str_replace('\\', '/', $controllerName);
        }

        private function  getMatchController($controllers, $currentController):bool
        {

            return in_array($currentController, $controllers);
        }
        private function applyController(string $controllerName):bool
        {
          
            $fullControllerName = $this->prepareForPath($controllerName).\PHP_EXTENSION;

            $path = CONTROLLER_DIR.DS.$fullControllerName;
            if(!\file($path)){
                throw new Exception('Controller path not found');
            }
           
            
            include_once ($path);
            return true;
        }
        
        private function getControllerNamespace(string $controller):string
        {
            
            $namespace = $this->prepareForNs($controller);
            
            return $namespace;
        }
        
        private static function getControllers():array
        {
          
            if(\file_exists(CONTROLLER_DIR)){
                $result = \scandir(CONTROLLER_DIR);
                if($result){
                    return array_reduce($result, function ($acc, $item){
                        $item = pathinfo($item, PATHINFO_FILENAME);
                       
                        list($name)= explode('.', $item);
                        if(!empty($name)){
                            $acc[] = $name;
                            
                        }
                        return $acc;
                      
                    },[] );
                }
            } else {
                throw new \Exception('Controllers folder was not found');
            }
        }
        
        function parse($path)
        {
            
            foreach($this->reg_paths as $regxp=> $keys){
                if(preg_match_all($regxp, $path, $res)){
                    $keys = explode('/', $keys);                  
                    foreach ($keys as $i => $key){ 

                        list($value) = $res[$i +1];                                             
                        $this->$key = $value;
                        

                    }
                }
            }
        }
    
}
