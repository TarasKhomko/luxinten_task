<?php 
namespace App\Core;

class Factory
{
    public static function create($type, $params =[])
    {
        $some = new $type($params);       
        return $some;
    }
}