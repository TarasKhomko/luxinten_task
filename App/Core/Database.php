<?php

namespace App\Core;

class Database 
{
	private static $conn = null;
	public  function connect()
	{
            if (self::$conn === null) {        
                $dsn = "mysql:host=localhost;dbname=my_first_blog";
                self::$conn = new \PDO($dsn, 'root', '');
            }
            return self::$conn;
	}
}