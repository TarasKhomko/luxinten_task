<?php
namespace App\Core;
abstract class Controller
{
    public function renderTemplate(string $viewName, array $params = []){
        $filePath = \realpath(TEMPLATE_DIR. DS. $viewName.\TAMPLATE_EXTENSION );
    
        if(!\file_exists($filePath)){
            throw new \Exception($filePath.'template file is not exists');
        }
        extract($params, EXTR_OVERWRITE);
        require_once ($filePath);

        return $this;
    }
}