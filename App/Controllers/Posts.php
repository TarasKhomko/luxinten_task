<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Post;

class Posts extends Controller {

    public function index() {

        $post = Post::all();
        return $this->renderTemplate('News/index', $post);
    }

    public function find($id) {
        $post = Post::find($id);
        return $this->renderTemplate('News/find', $post);
    }

    public function create() {
        return $this->renderTemplate('News/create');
    }

    public function store() {

        $title = $_POST["title"];
        $article = $_POST["article"];

        $data = [$title, $article];
        $post = Post::create($data);
        header('Location: /luxinten_task/');
    }

    public function edit($id) {
        $post = Post::find($id);
        return $this->renderTemplate('News/edit', $post);
    }

    public function update() {
        $id = $_POST["id"];
        $title = $_POST["title"];
        $article = $_POST["article"];
        $data = ['id' => $id,
            'title' => $title,
            'article' => $article];
        $post = Post::update($data);
        header('Location: /luxinten_task/');
    }

    public function delete($id) {
        $post = Post::delete($id);
        header('Location: /luxinten_task/');
    }

}
